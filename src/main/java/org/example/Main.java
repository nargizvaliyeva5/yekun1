package org.example;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.Duration;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class Main {
    WebDriver driver;
    WebDriverWait wait;
    JavascriptExecutor js;
    Actions action;
    @BeforeMethod
    public void setUp() {
        driver = new ChromeDriver();
        wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        js = (JavascriptExecutor) driver;
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
        driver.get("https://demoqa.com/");
        driver.manage().window().maximize();
    }
    @Test(priority = 1)
    public void checkTitle() {
        String title = driver.getTitle();
        Assert.assertEquals(title, "DEMOQA", "Title is not as expected");
    }
    @Test(priority = 2)
    public void checkInformationAfterSubmit() {
        List<WebElement> cards = driver.findElements(By.cssSelector(".category-cards>div"));
        WebElement cardElements = cards.get(0);
        js.executeScript("arguments[0].scrollIntoView();", cardElements);
        cardElements.click();
        List<WebElement> listItems = driver.findElements(By.cssSelector("ul>#item-0"));
        WebElement textBox = listItems.get(0);
        textBox.click();
        WebElement fullNameInput = driver.findElement(By.cssSelector("#userName"));
        fullNameInput.sendKeys("Nargiz Valiyeva");
        WebElement emailInput = driver.findElement(By.cssSelector("#userEmail"));
        emailInput.sendKeys("nara@gmail.com");
        WebElement currentAddressInput = driver.findElement(By.cssSelector("textarea[id=\"currentAddress\"]"));
        currentAddressInput.sendKeys("Baku, Azerbaijan");
        WebElement permanentAddressInput = driver.findElement(By.cssSelector("textarea[id=\"permanentAddress\"]"));
        permanentAddressInput.sendKeys("Baku, Azerbaijan");
        WebElement submitBtn = driver.findElement(By.cssSelector("#submit"));
        js.executeScript("arguments[0].scrollIntoView();", submitBtn);
        submitBtn.click();

        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.cssSelector(".mb-1")));

        WebElement nameOutput = driver.findElement(By.cssSelector("#name"));
        WebElement emailOutput = driver.findElement(By.cssSelector("#email"));
        WebElement currentAddressOutput = driver.findElement(By.cssSelector("p#currentAddress"));
        WebElement permanentAddressOutput = driver.findElement(By.cssSelector("p#permanentAddress"));

        Assert.assertEquals(nameOutput.getText(), "Name:Nargiz Valiyeva", "Name is not as expected");
        Assert.assertEquals(emailOutput.getText(), "Email:nara@gmail.com", "Email is not as expected");
        Assert.assertEquals(currentAddressOutput.getText(), "Current Address :Baku, Azerbaijan", "Current address is not as expected");
        Assert.assertEquals(permanentAddressOutput.getText(), "Permananet Address :Baku, Azerbaijan", "Permanent address is not as expected");
    }
    @Test(priority = 3)
    public void checkNotesIsSelected() {
        List<WebElement> cards = driver.findElements(By.cssSelector(".category-cards>div"));
        WebElement cardElements = cards.get(0);
        js.executeScript("arguments[0].scrollIntoView();", cardElements);
        cardElements.click();
        List<WebElement> listItems = driver.findElements(By.cssSelector("ul>#item-1"));
        WebElement checkBox = listItems.get(0);
        checkBox.click();
        WebElement expandBtn = driver.findElement(By.cssSelector("button[title=\"Expand all\"]"));
        expandBtn.click();
        WebElement notesIcon = driver.findElement(By.cssSelector("label[for=\"tree-node-notes\"]>.rct-checkbox"));
        notesIcon.click();
        WebElement result = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(".text-success")));
        Assert.assertTrue(result.isDisplayed());
        // You can check the version I've commented
        //Assert.assertEquals(result.getText(), "notes", "Notes is not selected");
    }
    @Test(priority = 4)
    public void checkRightClickMeButton() {
        List<WebElement> cards = driver.findElements(By.cssSelector(".category-cards>div"));
        WebElement cardElements = cards.get(0);
        js.executeScript("arguments[0].scrollIntoView();", cardElements);
        cardElements.click();
        List<WebElement> listItems = driver.findElements(By.cssSelector("ul>#item-4"));
        WebElement buttons = listItems.get(0);
        buttons.click();
        WebElement rightClickBtn = driver.findElement(By.cssSelector("#rightClickBtn"));
        action = new Actions(driver);
        action.contextClick(rightClickBtn).build().perform();
        WebElement rightClickMessage = driver.findElement(By.cssSelector("#rightClickMessage"));
        Assert.assertTrue(rightClickMessage.isDisplayed());
    }
    @Test(priority = 5)
    public void checkFileUploaded() {
        List<WebElement> cards = driver.findElements(By.cssSelector(".category-cards>div"));
        WebElement cardElements = cards.get(0);
        js.executeScript("arguments[0].scrollIntoView();", cardElements);
        cardElements.click();
        List<WebElement> listItems = driver.findElements(By.cssSelector("ul>#item-7"));
        WebElement uploadAndDownload = listItems.get(0);
        js.executeScript("arguments[0].scrollIntoView();", uploadAndDownload);
        uploadAndDownload.click();
        WebElement uploadFile = driver.findElement(By.cssSelector("#uploadFile"));
        String path = "C:\\Users\\Nergiz\\IdeaProjects\\avtotestt\\src\\main\\resources\\text.txt";
        uploadFile.sendKeys(path);
        WebElement text = driver.findElement(By.cssSelector("#uploadedFilePath"));
        wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#uploadedFilePath")));
        Assert.assertTrue(text.isDisplayed());
    }
    @Test(priority = 6)
    public void checkVisibilityOfTheButton() {
        List<WebElement> cards = driver.findElements(By.cssSelector(".category-cards>div"));
        WebElement cardElements = cards.get(0);
        js.executeScript("arguments[0].scrollIntoView();", cardElements);
        cardElements.click();
        List<WebElement> listItems = driver.findElements(By.cssSelector("ul>#item-8"));
        WebElement dynamicProperties = listItems.get(0);
        js.executeScript("arguments[0].scrollIntoView();", dynamicProperties);
        dynamicProperties.click();
        WebElement visibleAfterBtn = driver.findElement(By.cssSelector("#visibleAfter"));
        wait.until(ExpectedConditions.visibilityOf(visibleAfterBtn));
        Assert.assertTrue(visibleAfterBtn.isDisplayed());
    }
    @Test(priority = 7)
    public void checkNewTabIsOpened() {
        List<WebElement> cards = driver.findElements(By.cssSelector(".category-cards>div"));
        WebElement cardAlertsFrameWindows = cards.get(2);
        js.executeScript("arguments[0].scrollIntoView();", cardAlertsFrameWindows);
        cardAlertsFrameWindows.click();
        List<WebElement> listItems = driver.findElements(By.cssSelector(".menu-list>#item-0"));
        WebElement browserWindows = listItems.get(2);
        browserWindows.click();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
        WebElement newTabBtn = driver.findElement(By.cssSelector("#tabButton"));
        newTabBtn.click();

        String mainWindow = driver.getWindowHandle();
        Set<String> windowsId = driver.getWindowHandles();
        Iterator<String> iterator = windowsId.iterator();
        while(iterator.hasNext()) {
            String childWindow = iterator.next();
            if(!mainWindow.equalsIgnoreCase(childWindow)) {
                driver.switchTo().window(childWindow);
                WebElement h1 = driver.findElement(By.cssSelector("#sampleHeading"));
                Assert.assertTrue(h1.isDisplayed(),"This is not a new tab");
                driver.close();
            }
        }
        driver.quit();
    }
    @Test(priority = 8)
    public void checkInformationWrittenInPromptBox() {
        List<WebElement> cards = driver.findElements(By.cssSelector(".category-cards>div"));
        WebElement cardAlertsFrameWindows = cards.get(2);
        js.executeScript("arguments[0].scrollIntoView();", cardAlertsFrameWindows);
        cardAlertsFrameWindows.click();
        List<WebElement> listItems = driver.findElements(By.cssSelector(".menu-list>#item-1"));
        WebElement alerts = listItems.get(1);
        alerts.click();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
        WebElement promptBtn = driver.findElement(By.cssSelector("#promtButton"));
        promptBtn.click();
        driver.switchTo().alert().sendKeys("Upsss");
        driver.switchTo().alert().accept();
        WebElement promptResult = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#promptResult")));
        Assert.assertEquals(promptResult.getText(), "You entered Upsss", "The information does not match");
        // You can also check the version I've commented
        //Assert.assertTrue(promptResult.getText().equals("You entered Upsss"), "The information does not match");
    }
    @Test(priority = 9)
    public void checkAlertCanceled() {
        List<WebElement> cards = driver.findElements(By.cssSelector(".category-cards>div"));
        WebElement cardAlertsFrameWindows = cards.get(2);
        js.executeScript("arguments[0].scrollIntoView();", cardAlertsFrameWindows);
        cardAlertsFrameWindows.click();
        List<WebElement> listItems = driver.findElements(By.cssSelector(".menu-list>#item-1"));
        WebElement alerts = listItems.get(1);
        alerts.click();
        WebElement confirmBtn = driver.findElement(By.cssSelector("#confirmButton"));
        confirmBtn.click();
        driver.switchTo().alert().dismiss();
        WebElement confirmResult = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#confirmResult")));
        Assert.assertTrue(confirmResult.getText().equals("You selected Cancel"));
        // You can also check the version I've commented
        //Assert.assertEquals(confirmResult.getText(), "You selected Cancel", "The information does not match");

    }
    @Test(priority = 10)
    public void checkTextInTheFrame() {
        List<WebElement> cards = driver.findElements(By.cssSelector(".category-cards>div"));
        WebElement cardAlertsFrameWindows = cards.get(2);
        js.executeScript("arguments[0].scrollIntoView();", cardAlertsFrameWindows);
        cardAlertsFrameWindows.click();
        List<WebElement> listItems = driver.findElements(By.cssSelector(".menu-list>#item-2"));
        WebElement frames = listItems.get(1);
        js.executeScript("arguments[0].scrollIntoView();", frames);
        frames.click();
        WebElement iframe = driver.findElement(By.cssSelector("iframe#frame1"));
        driver.switchTo().frame(iframe);
        WebElement h1 = driver.findElement(By.cssSelector("#sampleHeading"));
        Assert.assertTrue(h1.isDisplayed());
    }
    @Test(priority = 11)
    public void checkFiveIsChose() {
        List<WebElement> cards = driver.findElements(By.cssSelector(".category-cards>div"));
        WebElement cardInteractions = cards.get(4);
        js.executeScript("arguments[0].scrollIntoView();", cardInteractions);
        cardInteractions.click();
        List<WebElement> listItems = driver.findElements(By.cssSelector(".menu-list>#item-1"));
        WebElement selectable = listItems.get(3);
        js.executeScript("arguments[0].scrollIntoView();", selectable);
        selectable.click();
        WebElement grid = driver.findElement(By.cssSelector("a[id=\"demo-tab-grid\"]"));
        grid.click();
        List<WebElement> gridListItems = driver.findElements(By.cssSelector("#row2>li"));
        WebElement five = gridListItems.get(1);
        five.click();
        wait.until(ExpectedConditions.attributeToBe(five, "class", "list-group-item active list-group-item-action"));
        Assert.assertEquals(five.getAttribute("class"), "list-group-item active list-group-item-action", "Five is not chosen");
    }
    @Test(priority = 12)
    public void checkDraggedAndDropped() {
        List<WebElement> cards = driver.findElements(By.cssSelector(".category-cards>div"));
        WebElement cardInteractions = cards.get(4);
        js.executeScript("arguments[0].scrollIntoView();", cardInteractions);
        cardInteractions.click();
        List<WebElement> listItems = driver.findElements(By.cssSelector(".menu-list>#item-3"));
        WebElement droppable = listItems.get(3);
        js.executeScript("arguments[0].scrollIntoView();", droppable);
        droppable.click();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
        WebElement dragMe = driver.findElement(By.cssSelector("#draggable"));
        WebElement dropHere = driver.findElement(By.cssSelector("#droppable"));
        action = new Actions(driver);
        action.dragAndDrop(dragMe, dropHere).build().perform();
        List<WebElement> texts = driver.findElements(By.cssSelector("div[id=\"droppable\"]>p"));
        WebElement textDropped = texts.get(0);
        Assert.assertEquals(textDropped.getText(), "Dropped!", "Drag me is not dropped!");
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
        // You can also check the version I've commented
        //Assert.assertTrue(textDropped.getText().equals("Dropped"));
    }
    @AfterMethod
    public void tearDown() {
        driver.quit();
    }
}
